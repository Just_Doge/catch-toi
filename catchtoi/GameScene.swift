//
//  GameScene.swift
//  catchtoi
//
//  Created by Geddy on 23/4/17.
//  Copyright © 2017 Smilez. All rights reserved.
//

import SpriteKit
import GameplayKit

var player = SKSpriteNode()
var collectable = SKSpriteNode()

var playerSize = CGSize(width: 80, height: 80)
var collectableSize = CGSize(width: 30, height: 30)

var lblMain = SKLabelNode()
var lblScore = SKLabelNode()

var touchLocation = CGPoint()

var score = 0

var playerSpeed = 1.0

var timeCollectableSpawn = 0.7

var timeToCountDown = 17
var isAlive = true

var offBlackColor = UIColor(red: 0.15, green: 0.15, blue: 0.15, alpha: 1.0)
var offWhiteColor = UIColor(red: 0.98, green: 0.98, blue: 0.98, alpha: 1.0)

var orange = UIColor.orange
var green = UIColor.green
var purple = UIColor.purple
var blue = UIColor.blue
var red = UIColor.red

struct PhysicsCategory {
    static let player: UInt32 = 1
    static let collectable: UInt32 = 2
    static let enemy: UInt32 = 3
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    override func didMove(to view: SKView) {
        physicsWorld.contactDelegate = self
        self.backgroundColor = orange
        
        spawnPlayer()
        spawnLblScore()
        spawnLblMain()
        spawnCollectableTimer()
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for touch in touches {
            touchLocation = touch.location(in: self)
            
            rotatePlayer()
            movePlayerToTouch()
            
        }
        
    }
    
    func rotatePlayer() {
        let angle = atan2(touchLocation.y - player.position.y, touchLocation.x - player.position.x)
        player.zRotation = angle - CGFloat(M_PI_2)
    }
    
    func movePlayerToTouch() {
        let moveForward = SKAction.move(to: touchLocation, duration: playerSpeed)
        let sequence = SKAction.sequence([moveForward])
        
        player.run(SKAction.repeat(sequence, count: 1))
    }
    
    func spawnPlayer() {
        player = SKSpriteNode(color: offWhiteColor, size: playerSize)
        player.size = playerSize
        
        player.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        player.physicsBody = SKPhysicsBody(rectangleOf: player.size)
        player.physicsBody?.affectedByGravity = false
        player.physicsBody?.allowsRotation = false
        player.physicsBody?.categoryBitMask = PhysicsCategory.player
        player.physicsBody?.contactTestBitMask = PhysicsCategory.collectable
        player.physicsBody?.isDynamic = false
        
        player.name = "playerName"
        
        self.addChild(player)
    }
    
    func spawnCollectableLeft() {
        
        let randomX = Int(arc4random_uniform(UInt32(self.frame.width)) + 1)
        let randomY = Int(arc4random_uniform(UInt32(self.frame.height)) + 1)
        collectable = SKSpriteNode(color: offBlackColor, size: collectableSize)
        collectable.size = collectableSize
        
        collectable.position = CGPoint(x: randomX, y: randomY)
        
        collectable.physicsBody = SKPhysicsBody(rectangleOf: collectable.size)
        collectable.physicsBody?.affectedByGravity = false
        collectable.physicsBody?.allowsRotation = false
        collectable.physicsBody?.categoryBitMask = PhysicsCategory.collectable
        collectable.physicsBody?.contactTestBitMask = PhysicsCategory.player
        
        collectable.physicsBody?.isDynamic = true
        collectable.zPosition = 2
        
        collectable.name = "collectableName"
        
        self.addChild(collectable)
    }
    
    func spawnCollectableRight() {
        
        let randomX = -1 * Int(arc4random_uniform(UInt32(self.frame.width)) + 1)
        let randomY = -1 * Int(arc4random_uniform(UInt32(self.frame.height)) + 1)
        collectable = SKSpriteNode(color: offBlackColor, size: collectableSize)
        collectable.size = collectableSize
        
        collectable.position = CGPoint(x: randomX, y: randomY)
        
        collectable.physicsBody = SKPhysicsBody(rectangleOf: collectable.size)
        collectable.physicsBody?.affectedByGravity = false
        collectable.physicsBody?.allowsRotation = false
        collectable.physicsBody?.categoryBitMask = PhysicsCategory.collectable
        collectable.physicsBody?.contactTestBitMask = PhysicsCategory.player
        
        collectable.physicsBody?.isDynamic = true
        collectable.zPosition = 2
        
        collectable.name = "collectableName"
        
        self.addChild(collectable)
    }
    
    func spawnLblMain() {
        lblMain = SKLabelNode(fontNamed: "Futura")
        lblMain.fontColor = offWhiteColor
        lblMain.fontSize = 60
        lblMain.position = CGPoint(x: self.frame.midX, y: self.frame.midY + 250)
        lblMain.text = ""
        
        self.addChild(lblMain)
        
        
    }
    
    func spawnLblScore() {
        lblScore = SKLabelNode(fontNamed: "Futura")
        lblScore.fontColor = offWhiteColor
        lblScore.fontSize = 40
        lblScore.position = CGPoint(x: self.frame.midX, y: self.frame.minY + 60)
        lblScore.text = "Score: \(score)"
        lblScore.zPosition = 3
        
        self.addChild(lblScore)
    }
    
    func spawnCollectableTimer() {
        let wait = SKAction.wait(forDuration: timeCollectableSpawn)
        let spawn = SKAction.run {
            if isAlive == true {
                self.spawnCollectableLeft()
                self.spawnCollectableRight()
            }
        }
        
        let sequence = SKAction.sequence([wait, spawn])
        self.run(SKAction.repeatForever(sequence))
    }
    
    func updateScore() {
        lblScore.text = "Score: \(score)"
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        let firstBody = contact.bodyA
        let secondBody = contact.bodyB
        
        if ((firstBody.categoryBitMask == PhysicsCategory.player) && (secondBody.categoryBitMask == PhysicsCategory.collectable) || (firstBody.categoryBitMask == PhysicsCategory.collectable) && (secondBody.categoryBitMask == PhysicsCategory.player)) {
            playerCollectableLogic(contactA: firstBody.node as! SKSpriteNode, contactB: secondBody.node as! SKSpriteNode)
        }
    }
    
    func playerCollectableLogic(contactA: SKSpriteNode, contactB: SKSpriteNode) {
        if contactA.name == "playerName" && contactB.name == "collectableName" {
            score += 1
            updateScore()
            contactB.removeFromParent()
        } else if contactA.name == "collectableName" && contactB.name == "playerName" {
            score += 1
            updateScore()
            contactA.removeFromParent()
        }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
